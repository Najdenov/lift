#include "stdafx.h"

Queue::Item::Item() :data(NULL), next(NULL), prev(NULL) {}

Queue::Item::Item(Request *data)
{
	this->data = data;
	next = NULL;
}

void Queue::Item::Data(Request *data)
{
	this->data = data;
}

Request& Queue::Item::Data()
{
	return *this->data;
}

Request* Queue::Item::Data_()
{
	return this->data;
}

void Queue::Item::Print()
{
	data->Print_R();
}

Queue::Queue(): head(NULL), tail(NULL), size(0){}
Queue::~Queue()
{
	DeleteAll();
	cout << "Delete Queue\n";
}

void Queue::Add(Request* data)
{
	Item *temp = new Item(data);
	
	if (IsEmpty())
		head = tail = temp;
	else if (size == 1)
	{
		tail = temp;
		tail->prev = head;
		head->next = tail;
	}
	else
	{
		Item *temp2 = new Item();
		tail->next = temp;
		temp2 = tail;
		tail = temp;
		tail->prev = temp2;
	}
	size++;
}

Request* Queue::Pop(Item* item)
{
	//tail->next = head;
	//tail = head;
	head = head->next;
	delete head;
	tail->next = NULL;
	return &tail->Data();
}

Queue::Item* Queue::Get_Head()
{
	return head;
}

void Queue::DeleteAll()
{
	Item *temp;
	while (head)
	{
		temp = head;
		head = head->next;
		delete temp;
	}
	size = 0;
}

bool Queue::IsEmpty() const
{
	return size == 0 ? true : false;
}

rsize_t Queue::Count() const
{
	return size;
}

void Queue::Print()
{
	Item *temp = head;
	while (temp != NULL)
	{
		temp->Print();
		temp = temp->next;
	}
}