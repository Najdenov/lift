#include "stdafx.h"

Lift::Lift()
{
	doorState = close;
	maxStage = 20;
	curCapacityKg = curPassangersCapacity = isWork = isBusy = 0;
	curStage = 1;
	maxCapacityKg = 450;
	maxCapacityUnit = 5;
	total_requests = new Queue;
	requests_lift = new Queue;
	Go_up = false;
	Go_down = false;
}
            
Lift::~Lift()
{
	delete total_requests;
	delete requests_lift;
}

int Lift::GetCurStage()
{
	return curStage;
}

int Lift::GetMaxStage()
{
	return maxStage;
}

int Lift::GetCurCapacityKg()
{
	return curCapacityKg;
}

int Lift::GetMaxCapacityKg()
{
	return maxCapacityKg;
}

int Lift::GetCurPassangersCapacity()
{
	return curPassangersCapacity;
}

int Lift::GetMaxCapacityUnit()
{
	return maxCapacityUnit;
}

Request* Lift::TotalRequests_Get_Current()
{
	return total_requests->Get_Head_Request();
}

Queue::Item* Lift::GetRequests_lift()
{
	return requests_lift;
}

Lift::DoorState Lift::IsDoorOpenOrClose()
{
	return doorState;
}

void Lift::OpenDoor()
{
	if(doorState == close)
	{
		cout<<"����� �����������\n";
		doorState = open;
	}
	else
		cout<<"����� ���������� �������, �.�. ��� �������\n";
}

void Lift::CloseDoor()
{
	if(doorState == close)
		cout<<"����� �������\n";
	else
	{
		cout<<"����� �����������\n";
		doorState = close;
	}
}

bool Lift::IsLiftFull()
{
	if (curCapacityKg == maxCapacityKg || curPassangersCapacity == maxCapacityUnit)
	{
		true;
	}
	return false;
}

bool Lift::IsLiftFull(int weight)
{
	return curCapacityKg + weight>= maxCapacityKg || curPassangersCapacity + 1 >= maxCapacityUnit ?
		false : true;
}

void Lift::StageUp(int curPassangerStage)
{
	while (1)
	{
		if (!IsmaxStage())
		{
			if (curStage != curPassangerStage)
			{
				curStage++;
				cout << curStage << " ����";
				Go_up = true;
				Sleep(1000);
			}
			else
			{
				Go_up = false;
				break;
			}
		}
		else
		{
			cout << "��������� ����������� ��������� ����\n";
			isWork = false;
			break;
		}
	}
}

void Lift::StageDown(int curPassangerStage)
{
	while (1)
	{
		if (!IsLowStage())
		{
			if (curStage != curPassangerStage)
			{
				curStage--;
				cout << curStage << " ����";
				Go_down = true;
				Sleep(1000);
			}
			else
			{
				Go_down = false;
				break;
			}
		}
		else
		{
			cout << "��������� ���������� ��������� ����";
			isWork = false;
			break;
		}
	}
}

bool Lift::IsmaxStage()
{
	return curStage == maxStage ? true : false;
}

bool Lift::IsLowStage()
{
	return curStage == 1 ? true : false;
}

void Lift::Add_request(Passanger* passanger)//Request *A)
{
	Request request(passanger);
	total_requests->Add(&request);
}

void Lift::Add_request_to_request_lift(Request *A)
{
	requests_lift->Add(A);
}

Queue& Lift::LoadQueue()
{
	return (Queue &) total_requests;
}
void Lift::Print()
{
	total_requests->Print();
}