#pragma once

class Lift
{
public:
	enum DoorState
	{
		open,
		close
	};
private:
	int curStage;					//������� ����
	int maxStage;					//������������ ����������� ������
	int curCapacityKg;				//������� ��������������� � ��
	int maxCapacityKg;				//������������ ��������������� � ��
	int curPassangersCapacity;		//������� ���������� ����������
	int maxCapacityUnit;			//������������ ��������������� �����
	Queue* total_requests;			//������ ���� ������
	Queue* requests_lift;			//������ ������ ����������� � �����
	bool Go_up;
	bool Go_down;
	DoorState doorState;			//��������� �����
public:
	Lift(void);
	~Lift(void);
	int GetCurStage();
	int GetMaxStage();
	int GetCurCapacityKg();
	int GetMaxCapacityKg();
	int GetCurPassangersCapacity();
	int GetMaxCapacityUnit();

	Queue::Item* TotalRequests_Get_Current();
	Queue::Item* GetRequests_lift();

	DoorState IsDoorOpenOrClose();
	bool IsLiftFull();
	bool IsLiftFull(int);
	bool isWork;					//�������� �� � ������ ������ ����
	bool isBusy;					//����� �� � ������ ������ ����
	void OpenDoor();
	void CloseDoor();
	void StageUp(int);
	void StageDown(int);
	bool IsmaxStage();
	bool IsLowStage();
	void Add_request(Passanger*);//Request*);
	void Add_request_to_request_lift(Request*);
	Queue& LoadQueue();
	void Print();
};