#pragma once

class Queue
{
public:
	class Item
	{
		Request *data;
	public:
		Item *next;
		Item *prev;
		Item();
		Item(Request *);
		void Data(Request *);
		Request& Data();
		Request* Data_();
		void Print();
	};
private:
	Item *head;
	Item *tail;
	size_t size;
public:
	Queue();
	~Queue();
	void Add(Request *);
	Request *Pop(Item*);
	Item* Get_Head();
	void DeleteAll();
	bool IsEmpty() const;
	size_t Count() const;
	void Print();
};