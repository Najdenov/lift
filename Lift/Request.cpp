#include "stdafx.h"

Request::Request(Passanger* passanger)
{
	passange_r = passanger;
	CurrentFloor();
}

Request::~Request(void)
{
	delete passange_r;
}

void Request::CurrentFloor()
{
	int destFloor = passange_r->Target_Floor();
	do{
		currentFloor = rand() % 20 + 1;
	} while (destFloor == currentFloor);
}

int& Request::Get_cur_floor()
{
	return currentFloor;
}

int& Request::Get_dest_floor()
{
	return passange_r->Target_Floor();
}

void Request::Print_R()
{
	passange_r->Print();
	cout<<"Current floor: "<<currentFloor<<"\n\n";
}

Passanger& Request::Get_passanger()
{
	return *passange_r;
}